/*
 * Copyright 2017 Teamninjaneer.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.squiggle.examples;

import com.squiggle.SimpleSquiggle;
import com.squiggle.SimpleSquiggleCoordinate;
import com.squiggle.SquiggleCoordinate;
import com.squiggle.axes.LinearXAxis;
import com.squiggle.axes.LinearYAxis;
import com.squiggle.controls.SquigglePlot;
import com.squiggle.render.RenderingAttributes;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * Example of using the waveform plot to put a waveform into a scene
 *
 * @author Chris Heidt <chris.heidt@vencore.com>
 */
public class SquigglePlotExample extends Application {

    private static final int SQUIGGLE_COUNT = 100;
    private static final int SQUIGGLE_COORDINATE_COUNT = 10000;
    private static final int SPACER = 10;

    @Override
    public void start(Stage stage) throws Exception {
        SquigglePlot plot = new SquigglePlot("My Charts Label");
        plot.setControlsEnabled(true);
        plot.setAxisEnabled(true);
        plot.setCrosshairsEnabled(true);

        plot.addXAxis(new LinearXAxis("My X Axis"));
        plot.addYAxis(new LinearYAxis("My Y Axis"));

        new Thread(() -> {
            Random random = new Random();
            for (int i = 0; i < SQUIGGLE_COUNT; i++) {
                List<SquiggleCoordinate> coordinates = new ArrayList<>();
                for (int j = 0; j < SQUIGGLE_COORDINATE_COUNT; j++) {
                    double x = j * SPACER;
                    double y = (i * SPACER) + (Math.sin(x) * SPACER);
                    coordinates.add(new SimpleSquiggleCoordinate(x, y));
                }

                Color randColor = randColor(random);
                SimpleSquiggle squiggle = new SimpleSquiggle("Squiggle " + i, coordinates);
                squiggle.setRenderingAttributes(new RenderingAttributes.Builder()
                        .withStrokePaint(randColor)
                        .withFillPaint(randColor)
                        .withLineWidth(2)
                        .build());
                plot.addSquiggle(squiggle);
            }
        }).start();

        //use standed fx sizing and styling
        plot.setPrefSize(1152, 648);
        plot.setStyle("-fx-background-color: BLACK;");

        //standard stage stuff
        stage.setTitle("Squiggle: Basic Waveform Plot Example");
        stage.setScene(new Scene(plot));
        stage.show();

    }

    private Color randColor(Random random) {
        return new Color(random.nextDouble(), random.nextDouble(), random.nextDouble(), 1);
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application. main() serves only as
     * fallback in case the application can not be launched through deployment artifacts, e.g., in
     * IDEs with limited FX support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
