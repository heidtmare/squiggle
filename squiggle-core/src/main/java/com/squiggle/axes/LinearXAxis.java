/*
 * Copyright 2017 Teamninjaneer.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.squiggle.axes;

import com.squiggle.LazyPropertyWrapper;
import com.squiggle.render.Coordinate;
import com.squiggle.render.RenderableBase;
import com.squiggle.render.RenderingContext;
import java.text.DecimalFormat;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Chris Heidt <chris.heidt@vencore.com>
 */
public class LinearXAxis extends RenderableBase implements XAxis {

    private static final Logger LOGGER = LoggerFactory.getLogger(LinearXAxis.class);
    private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#.###");
    private static final double AXIS_WIDTH = 20.5;
    private static final double MAJOR_TICK_LENGTH = 6;
    private static final double MINOR_TICK_LENGTH = 3;
    private final LazyPropertyWrapper<String> nameLazyProperty;
    private Anchor anchor = Anchor.VALUE;
    private double majorTickStep = 100d;
    private double minorTickStep = 10d;

    public LinearXAxis(String name) {
        nameLazyProperty = new LazyPropertyWrapper<>(SimpleStringProperty.class, name);
    }

    @Override
    public Property<String> nameProperty() {
        return nameLazyProperty.getProperty();
    }

    @Override
    public String getName() {
        return nameLazyProperty.getValue();
    }

    @Override
    public void setName(String name) {
        nameLazyProperty.setValue(name);
    }

    public Anchor getAnchor() {
        return anchor;
    }

    public void setAnchor(Anchor anchor) {
        this.anchor = anchor;
    }

    public double getMajorTickStep() {
        return majorTickStep;
    }

    public void setMajorTickStep(double majorTickStep) {
        this.majorTickStep = majorTickStep;
    }

    public double getMinorTickStep() {
        return minorTickStep;
    }

    public void setMinorTickStep(double minorTickStep) {
        this.minorTickStep = minorTickStep;
    }

    @Override
    public void render(RenderingContext ctx) {
        long startTime = System.nanoTime();

        Coordinate plotOrigin = ctx.getPlotOrigin();
        double axisMinX = plotOrigin.getX();
        double axisMinY = plotOrigin.getY() + ctx.getPlotHeight();
        double axisMaxX = plotOrigin.getX() + ctx.getPlotWidth();
        double axisMaxY = ctx.getScreenHeight();
        double minorY = axisMinY + MINOR_TICK_LENGTH;
        double majorY = axisMinY + MAJOR_TICK_LENGTH;

        GraphicsContext gc = ctx.getGraphicsContext();
        gc.save();

        //x axis background
        gc.setFill(Color.BLACK);
        //x axis extends into corner dead zones
        gc.fillRect(0, axisMinY, ctx.getScreenWidth(), ctx.getInsets().getBottom());

        //x axis line
        gc.setStroke(Color.WHITE);
        gc.beginPath();
        gc.moveTo(axisMinX, axisMinY);
        gc.lineTo(axisMaxX, axisMinY);

        double xOrigin;
        double xMinorStep;
        double xMajorStep;
        if (Anchor.SCREEN.equals(anchor)) {
            xOrigin = axisMinX;
            xMinorStep = getMinorTickStep();
            xMajorStep = getMajorTickStep();
        } else {
            xOrigin = ctx.getCartOrigin().getX();
            xMinorStep = getMinorTickStep() * ctx.getScaleX();
            xMajorStep = getMajorTickStep() * ctx.getScaleX();
        }

        //Minor ticks, no minor on the origin
        //yOrigin + yMinorStep -> yPlotMax
        for (double minorX = xOrigin + xMinorStep; minorX <= axisMaxX; minorX += xMinorStep) {
            if (minorX < axisMinX) {
                continue;
            }
            drawMinorTick(ctx, axisMinY, minorX, minorY);
        }
        //yOrigin - yMinorStep -> yPlotMin
        for (double minorX = xOrigin - xMinorStep; minorX >= axisMinX; minorX -= xMinorStep) {
            if (minorX > axisMaxX) {
                continue;
            }
            drawMinorTick(ctx, axisMinY, minorX, minorY);
        }

        //Major ticks.
        //yOrigin + yMajorStep -> yPlotMax
        for (double majorX = xOrigin + xMajorStep; majorX <= axisMaxX; majorX += xMajorStep) {
            if (majorX < axisMinX) {
                continue;
            }
            drawMajorTick(ctx, axisMinY, axisMaxY, majorX, majorY);
        }
        //yOrigin -> yPlotMin, includes origin
        for (double majorX = xOrigin; majorX >= axisMinX; majorX -= xMajorStep) {
            if (majorX > axisMaxX) {
                continue;
            }
            drawMajorTick(ctx, axisMinY, axisMaxY, majorX, majorY);
        }

        gc.stroke();

        gc.restore();

        LOGGER.debug("RENDERING XAXIS: {}", System.nanoTime() - startTime);
    }

    private void drawMinorTick(RenderingContext ctx, double axisMinY, double tickX, double tickY) {
        GraphicsContext gc = ctx.getGraphicsContext();
        gc.moveTo(tickX, axisMinY);
        gc.lineTo(tickX, tickY);
    }

    private void drawMajorTick(RenderingContext ctx, double axisMinY, double axisMaxY, double tickX, double tickY) {
        GraphicsContext gc = ctx.getGraphicsContext();
        gc.moveTo(tickX, axisMinY);
        gc.lineTo(tickX, tickY);

        double cartX = ctx.screenXToCartX(tickX);
        gc.setTextAlign(TextAlignment.CENTER);
        gc.setFill(Color.WHITE);
        gc.fillText(DECIMAL_FORMAT.format(cartX), tickX, axisMaxY);
    }
}
