/*
 * Copyright 2017 Teamninjaneer.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.squiggle.axes;

import com.squiggle.LazyPropertyWrapper;
import com.squiggle.render.Coordinate;
import com.squiggle.render.RenderableBase;
import com.squiggle.render.RenderingContext;
import java.text.DecimalFormat;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;

/**
 *
 * @author Chris Heidt <chris.heidt@vencore.com>
 */
public class LogrithmicYAxis extends RenderableBase implements YAxis {

    private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#.###");
    private static final double AXIS_WIDTH = 20.5;
    private static final double MAJOR_TICK_LENGTH = 6;
    private static final double MINOR_TICK_LENGTH = 3;
    private final LazyPropertyWrapper<String> nameLazyProperty;
    private Anchor anchor = Anchor.VALUE;
    private double majorTickStep = 100d;
    private double minorTickStep = 10d;

    public LogrithmicYAxis(String name) {
        nameLazyProperty = new LazyPropertyWrapper<>(SimpleStringProperty.class, name);
    }

    @Override
    public Property<String> nameProperty() {
        return nameLazyProperty.getProperty();
    }

    @Override
    public String getName() {
        return nameLazyProperty.getValue();
    }

    @Override
    public void setName(String name) {
        nameLazyProperty.setValue(name);
    }

    public Anchor getAnchor() {
        return anchor;
    }

    public void setAnchor(Anchor anchor) {
        this.anchor = anchor;
    }

    public double getMajorTickStep() {
        return majorTickStep;
    }

    public void setMajorTickStep(double majorTickStep) {
        this.majorTickStep = majorTickStep;
    }

    public double getMinorTickStep() {
        return minorTickStep;
    }

    public void setMinorTickStep(double minorTickStep) {
        this.minorTickStep = minorTickStep;
    }

    @Override
    public void render(RenderingContext ctx) {
        Coordinate screenOrigin = ctx.getScreenOrigin();
        Coordinate plotOrigin = ctx.getPlotOrigin();
        double axisMinX = screenOrigin.getX();
        double axisMinY = plotOrigin.getY();
        double axisMaxX = plotOrigin.getX();
        double axisMaxY = plotOrigin.getY() + ctx.getPlotHeight();
        double minorX = plotOrigin.getX() - MINOR_TICK_LENGTH;
        double majorX = plotOrigin.getX() - MAJOR_TICK_LENGTH;

        GraphicsContext gc = ctx.getGraphicsContext();
        gc.save();

        //y axis background
        gc.setFill(Color.BLACK);
        int offset = 1; //Magic Number: offset due to a small gap at the bottom on the background
        gc.fillRect(axisMinX, axisMinY, ctx.getInsets().getLeft(), axisMaxY + offset);

        //y axis line
        gc.setStroke(Color.WHITE);
        gc.beginPath();
        gc.moveTo(axisMaxX, axisMinY);
        gc.lineTo(axisMaxX, axisMaxY);

        double yOrigin;
        double yMinorStep;
        double yMajorStep;
        if (Anchor.SCREEN.equals(anchor)) {
            yOrigin = axisMaxY;
            yMinorStep = getMinorTickStep();
            yMajorStep = getMajorTickStep();
        } else {
            yOrigin = ctx.getCartOrigin().getY();
            yMinorStep = getMinorTickStep() * ctx.getScaleY();
            yMajorStep = getMajorTickStep() * ctx.getScaleY();
        }

        //Minor ticks, no minor on the origin
        //yOrigin + yMinorStep -> yPlotMax
        for (double minorY = yOrigin + yMinorStep; minorY <= axisMaxY; minorY += yMinorStep) {
            if (minorY < axisMinY) {
                continue;
            }
            drawMinorTick(ctx, axisMaxX, minorX, minorY);
        }
        //yOrigin - yMinorStep -> yPlotMin
        for (double minorY = yOrigin - yMinorStep; minorY >= axisMinY; minorY -= yMinorStep) {
            if (minorY > axisMaxY) {
                continue;
            }
            drawMinorTick(ctx, axisMaxX, minorX, minorY);
        }

        //Major ticks.
        //yOrigin + yMajorStep -> yPlotMax
        for (double majorY = yOrigin + yMajorStep; majorY <= axisMaxY; majorY += yMajorStep) {
            if (majorY < axisMinY) {
                continue;
            }
            drawMajorTick(ctx, axisMaxX, majorX, majorY);
        }
        //yOrigin -> yPlotMin, includes origin
        for (double majorY = yOrigin; majorY >= axisMinY; majorY -= yMajorStep) {
            if (majorY > axisMaxY) {
                continue;
            }
            drawMajorTick(ctx, axisMaxX, majorX, majorY);
        }

        gc.stroke();

        gc.restore();
    }

    private void drawMinorTick(RenderingContext ctx, double lineX, double tickX, double tickY) {
        GraphicsContext gc = ctx.getGraphicsContext();
        gc.moveTo(lineX, tickY);
        gc.lineTo(tickX, tickY);
    }

    private void drawMajorTick(RenderingContext ctx, double lineX, double tickX, double tickY) {
        GraphicsContext gc = ctx.getGraphicsContext();

        gc.moveTo(lineX, tickY);
        gc.lineTo(tickX, tickY);

        double cartY = ctx.screenToCart(0, tickY).getY();
        String yText = DECIMAL_FORMAT.format(cartY);

        gc.save();
        gc.translate(0, tickY);
        gc.rotate(90);
        gc.setTextAlign(TextAlignment.CENTER);
        gc.setFill(Color.WHITE);
        gc.fillText(yText, 0, 0);
        gc.restore();
    }
}
