/*
 * Copyright 2017 Teamninjaneer.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.squiggle;

import com.squiggle.render.Coordinate;
import com.squiggle.render.RenderableBase;
import com.squiggle.render.RenderingAttributes;
import com.squiggle.render.RenderingContext;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Chris Heidt <chris.heidt@vencore.com>
 */
public class SimpleSquiggle extends RenderableBase implements Squiggle {

    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleSquiggle.class);
    private final LazyPropertyWrapper<String> nameLazyProperty;
    private final List<SquiggleCoordinate> coordinates = new ArrayList<>();

    static {
        defaultRenderingAttributes = new RenderingAttributes.Builder().withLineWidth(2).build();
    }
    private boolean selected;

    public SimpleSquiggle() {
        this(null);
    }

    public SimpleSquiggle(String title, SquiggleCoordinate... coordinates) {
        nameLazyProperty = new LazyPropertyWrapper<>(SimpleStringProperty.class, title);
        Collections.addAll(this.coordinates, coordinates);
    }

    public SimpleSquiggle(String title, Collection<SquiggleCoordinate> coordinates) {
        nameLazyProperty = new LazyPropertyWrapper<>(SimpleStringProperty.class, title);
        this.coordinates.addAll(coordinates);
    }

    public void addCoordinates(Collection<SquiggleCoordinate> coordinates) {
        this.coordinates.addAll(coordinates);
    }

    public void addCoordinates(SquiggleCoordinate... coordinates) {
        Collections.addAll(this.coordinates, coordinates);
    }

    public void removeCoordinates(Collection<SquiggleCoordinate> coordinates) {
        this.coordinates.removeAll(coordinates);
    }

    public void removeCoordinates(SquiggleCoordinate... coordinates) {
        removeCoordinates(Arrays.asList(coordinates));
    }

    /**
     * Unmodifiable snapshot of coordinates
     *
     * @return
     */
    @Override
    public List<SquiggleCoordinate> getCoordinatesReadOnly() {
        return Collections.unmodifiableList(new ArrayList(coordinates));
    }

    @Override
    public double getMinX() {
        if (coordinates.isEmpty()) {
            return Double.NaN;
        }

        return coordinates.stream().mapToDouble(SquiggleCoordinate::getX).min().orElse(Double.NaN);
    }

    @Override
    public double getMaxX() {
        if (coordinates.isEmpty()) {
            return Double.NaN;
        }

        return coordinates.stream().mapToDouble(SquiggleCoordinate::getX).max().orElse(Double.NaN);
    }

    @Override
    public double getMinY() {
        if (coordinates.isEmpty()) {
            return Double.NaN;
        }

        return coordinates.stream().mapToDouble(SquiggleCoordinate::getY).min().orElse(Double.NaN);
    }

    @Override
    public double getMaxY() {
        if (coordinates.isEmpty()) {
            return Double.NaN;
        }

        return coordinates.stream().mapToDouble(SquiggleCoordinate::getY).max().orElse(Double.NaN);
    }

    @Override
    public Property<String> nameProperty() {
        return nameLazyProperty.getProperty();
    }

    @Override
    public String getName() {
        return nameLazyProperty.getValue();
    }

    @Override
    public void setName(String name) {
        nameLazyProperty.setValue(name);
    }

    @Override
    public void render(RenderingContext ctx) {
        long startTime = System.nanoTime();

        GraphicsContext gc = ctx.getGraphicsContext();
        gc.save();

        //apply rendering attributes
        if (renderingAttributes != null) {
            renderingAttributes.apply(gc);
        } else {
            defaultRenderingAttributes.apply(gc);
        }

        if (selected) {
            gc.setStroke(Color.CYAN);
            gc.setLineWidth(gc.getLineWidth() + 1);
        }

        //apply pick color
        if (ctx.isPicking()) {
            Color pickColor = ctx.registerPickable(this);
            gc.setStroke(pickColor);
            gc.setLineWidth(gc.getLineWidth() * 2);//makes picking lines easier
        }

        SquiggleCoordinate[] coordsCopy = coordinates.toArray(new SquiggleCoordinate[coordinates.size()]);
        double[] xArray = new double[coordsCopy.length];
        double[] yArray = new double[coordsCopy.length];
        int index = 0;
        double priorX = Double.NaN;
        double priorY = Double.NaN;
        for (SquiggleCoordinate coord : coordsCopy) {

            Coordinate screenCoordinate = ctx.cartToScreen(coord.getX(), coord.getY());
            double currentX = screenCoordinate.getX();
            double currentY = screenCoordinate.getY();

            //if its outside the screen buffer then just ignore it
            if (ctx.isCulled(currentX, currentY)) {
                continue;
            }

            //draw point
            if (selected) {
                coord.render(ctx);
            }

            //if we end up on the same screen x we average the y's together (simplestic optimation)
            if (!Double.isNaN(priorX) && Double.compare(priorX, currentX) == 0) {
                priorY = (priorY + currentY) / 2;
                yArray[index] = priorY;
                continue;
            }

            //track prior values
            priorX = currentX;
            priorY = currentY;

            //set screen values
            xArray[index] = currentX;
            yArray[index] = currentY;

            //increment
            index++;
        }

        //draw line
        gc.strokePolyline(xArray, yArray, index);

        //reset state
        gc.restore();

        LOGGER.debug("RENDERING SQUIGGLE: {}", System.nanoTime() - startTime);
    }

    @Override
    public void pick(Coordinate pickCoordinate) {
        selected = !selected;
    }
}
