/*
 * Copyright 2017 Teamninjaneer.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.squiggle.render;

import java.util.HashMap;
import java.util.Map;
import javafx.geometry.Insets;
import javafx.scene.SnapshotParameters;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Chris Heidt <chris.heidt@vencore.com>
 */
public class RenderingContext {

    private static final Logger LOGGER = LoggerFactory.getLogger(RenderingContext.class);

    private static final double CULLING_MARGIN = 100d;
    private final Map<String, Object> properties;
    private final GraphicsContext gc;

    private int colorRoot = 1;
    private Map<Color, Pickable> pickables = new HashMap<>();
    private Coordinate pickCoordinate = null;
    //private List<Pickable> picks = new ArrayList<>();

    private Insets insets = Insets.EMPTY;
    private double scaleX = 1.0;
    private double scaleY = 1.0;
    private double translateX = 0.0;
    private double translateY = 0.0;

    public RenderingContext(GraphicsContext gc) {
        this.gc = gc;
        this.properties = new HashMap<>();
    }

    public RenderingContext(GraphicsContext gc, RenderingContext orig) {
        this.gc = gc;
        this.properties = orig.properties;
//        this.colorRoot = orig.colorRoot;
//        this.pickColors = orig.pickColors;
//        this.pickCoordinate = orig.pickCoordinate;
//        this.picks = orig.picks;
        this.insets = orig.insets;
        this.scaleX = orig.scaleX;
        this.scaleY = orig.scaleY;
        this.translateX = orig.translateX;
        this.translateY = orig.translateY;
    }

    public static double snap(double val) {
        return ((int) val) + .5;
    }

    /**
     *
     * @param cX
     * @param cY
     * @return
     */
    public Coordinate cartToScreen(double cX, double cY) {
        return new Coordinate(cartXToScreenX(cX), cartYToScreenY(cY));
    }

    /**
     *
     * @param cX
     * @return
     */
    public double cartXToScreenX(double cX) {
        return getInsets().getLeft() + getTranslateX() + (getScaleX() * cX);
    }

    /**
     *
     * @param cY
     * @return
     */
    public double cartYToScreenY(double cY) {
        return getScreenHeight() - getInsets().getBottom() - getTranslateY() - (getScaleY() * cY);
    }

    /**
     *
     * @param sX
     * @param sY
     * @return
     */
    public Coordinate screenToCart(double sX, double sY) {
        return new Coordinate(screenXToCartX(sX), screenYToCartY(sY));
    }

    /**
     *
     * @param sX
     * @return
     */
    public double screenXToCartX(double sX) {
        return (sX - getTranslateX() - getInsets().getLeft()) / getScaleX();
    }

    /**
     *
     * @param sY
     * @return
     */
    public double screenYToCartY(double sY) {
        return (sY - getScreenHeight() + getInsets().getBottom() + getTranslateY()) / -getScaleY();
    }

    /**
     *
     * @param sX
     * @param sY
     * @return
     */
    public boolean isCulled(double sX, double sY) {
        return sX < 0 - CULLING_MARGIN || sX > getScreenWidth() + CULLING_MARGIN
                || sY < 0 - CULLING_MARGIN || sY > getScreenHeight() + CULLING_MARGIN;
    }

    public GraphicsContext getGraphicsContext() {
        return gc;
    }

    public Coordinate getScreenOrigin() {
        return Coordinate.ZERO;
    }

    public double getScreenHeight() {
        return gc.getCanvas().getHeight();
    }

    public double getScreenWidth() {
        return gc.getCanvas().getWidth();
    }

    public Coordinate getPlotOrigin() {
        return new Coordinate(getInsets().getLeft(), getInsets().getTop());
    }

    public double getPlotHeight() {
        return getScreenHeight() - getInsets().getTop() - getInsets().getBottom();
    }

    public double getPlotWidth() {
        return getScreenWidth() - getInsets().getLeft() - getInsets().getRight();
    }

    public Coordinate getCartOrigin() {
        return cartToScreen(0, 0);
    }

    public Insets getInsets() {
        return insets;
    }

    public void setInsets(Insets insets) {
        this.insets = insets;
    }

    public double getYRatio(double maxY) {
        return getScreenHeight() / maxY;
    }

    public double getXRatio(double maxX) {
        return getScreenWidth() / maxX;
    }

    public double getTranslateX() {
        return translateX;
    }

    public double getTranslateY() {
        return translateY;
    }

    public double getScaleX() {
        return scaleX;
    }

    public void setScaleX(double scaleX) {
        this.scaleX = scaleX;
    }

    public double getScaleY() {
        return scaleY;
    }

    public void setScaleY(double scaleY) {
        this.scaleY = scaleY;
    }

    public void setTranslateX(double offsetX) {
        this.translateX = offsetX;
    }

    public void setTranslateY(double offsetY) {
        this.translateY = offsetY;
    }

    public <V> V getProperty(String key) {
        return (V) properties.get(key);
    }

    public void setProperty(String key, Object value) {
        properties.put(key, value);
    }

    public boolean isPicking() {
        return pickCoordinate != null;
    }

    public Color registerPickable(Pickable pickable) {
        Color pickColor = nextColor();
        pickables.put(pickColor, pickable);
        return pickColor;
    }

    public void setPickCoordinate(Coordinate screenCoord) {
        LOGGER.debug("Pick Coordinate: " + screenCoord);
        this.pickCoordinate = screenCoord;
    }

    public Pickable resolvePick() {
        if (pickCoordinate == null) {
            return null;
        }

        WritableImage canvasSnapshot = gc.getCanvas().snapshot(
                new SnapshotParameters(),
                new WritableImage((int) getScreenWidth(), (int) getScreenHeight())
        );

        Color pixelColor = canvasSnapshot.getPixelReader().getColor((int) pickCoordinate.getX(), (int) pickCoordinate.getY());
        Pickable pickable = pickables.get(pixelColor);
        if (pickable == null) {
            return null;
        }

        return pickable;
    }

    /**
     *
     */
    public void clear() {
        //clear graphics context
        gc.save();
        gc.setTransform(1, 0, 0, 1, 0, 0);// Use the identity matrix while clearing the canvas
        gc.clearRect(0, 0, getScreenWidth(), getScreenHeight());
        gc.restore();
    }

    private Color nextColor() {
        //if we exhaust our available colors we start over and prey they no longer exist in the scene
        if (colorRoot == 16777215) {
            colorRoot = 1;
        }

        int r = (colorRoot & 0xff);
        int g = (colorRoot & 0xff00) >> 8;
        int b = (colorRoot & 0xff0000) >> 16;
        colorRoot++;

        return Color.rgb(r, g, b);
    }
}
