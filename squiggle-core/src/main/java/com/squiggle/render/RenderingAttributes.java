/*
 * Copyright 2017 Teamninjaneer.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.squiggle.render;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

/**
 *
 * @author Chris Heidt <chris.heidt@vencore.com>
 */
public final class RenderingAttributes {

    public static final Paint DEFAULT_FILL_PAINT = Color.WHITE;
    public static final Paint DEFAULT_STROKE_PAINT = Color.WHITE;
    public static final double DEFAULT_LINE_WIDTH = 1d;

    private final Paint fillPaint;
    private final Paint strokePaint;
    private final double lineWidth;

    //used by builder
    private RenderingAttributes(Paint fillPaint, Paint strokePaint, double lineWidth) {
        this.fillPaint = fillPaint;
        this.strokePaint = strokePaint;
        this.lineWidth = lineWidth;
    }

    public Paint getStrokePaint() {
        return strokePaint;
    }

    public Paint getFillPaint() {
        return fillPaint;
    }

    public double getLineWidth() {
        return lineWidth;
    }

    /**
     *
     * @param gc
     */
    public void apply(GraphicsContext gc) {
        gc.setStroke(strokePaint);
        gc.setFill(fillPaint);
        gc.setLineWidth(lineWidth);
    }

    public static class Builder {

        private Paint fillPaint = DEFAULT_FILL_PAINT;
        private Paint strokePaint = DEFAULT_STROKE_PAINT;
        private double lineWidth = DEFAULT_LINE_WIDTH;

        public Builder() {
            //empty
        }

        public Builder(RenderingAttributes renderingAttributes) {
            this.fillPaint = renderingAttributes.fillPaint;
            this.strokePaint = renderingAttributes.strokePaint;
            this.lineWidth = renderingAttributes.lineWidth;
        }

        public Builder withFillPaint(Paint fillPaint) {
            this.fillPaint = fillPaint;
            return this;
        }

        public Builder withStrokePaint(Paint strokePaint) {
            this.strokePaint = strokePaint;
            return this;
        }

        public Builder withLineWidth(double lineWidth) {
            this.lineWidth = lineWidth;
            return this;
        }

        public RenderingAttributes build() {
            return new RenderingAttributes(fillPaint, strokePaint, lineWidth);
        }
    }

}
