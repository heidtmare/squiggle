/*
 * Copyright 2017 Teamninjaneer.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.squiggle;

import javafx.beans.property.Property;
import javafx.beans.property.SimpleObjectProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Chris Heidt <chris.heidt@vencore.com>
 * @param <T>
 */
public class LazyPropertyWrapper<T> {

    private static final Logger LOGGER = LoggerFactory.getLogger(LazyPropertyWrapper.class);
    private final Class<? extends Property<T>> propertyClass;
    private Property<T> property;
    private T value;

    /**
     * Helper class to handle lazy creation of Properties
     *
     * @param propertyClass Property implementation to create on demand
     * @param value The initial value
     */
    public LazyPropertyWrapper(Class<? extends Property<T>> propertyClass, T value) {
        this.value = value;
        this.propertyClass = propertyClass;
    }

    /**
     *
     * @param value
     */
    public LazyPropertyWrapper(T value) {
        this.value = value;
        this.propertyClass = null;
    }

    public Property<T> getProperty() {
        if (property != null) {
            return property;
        }

        //use provided property type
        if (propertyClass != null) {
            try {
                property = propertyClass.newInstance();
                property.setValue(value);
                value = null;//mem management
                return property;
            } catch (InstantiationException | IllegalAccessException ex) {
                LOGGER.warn("Failed to generate and set new lazy property instance! ", ex.getMessage());
                //continue on to do object property default
            }
        }

        //default to object property
        property = new SimpleObjectProperty<>(value);
        value = null;//mem management
        return property;
    }

    /**
     *
     * @return
     */
    public T getValue() {
        return property == null ? value : property.getValue();
    }

    /**
     *
     * @param value
     */
    public void setValue(T value) {
        if (property == null) {
            this.value = value;
        } else {
            property.setValue(value);
        }
    }
}
