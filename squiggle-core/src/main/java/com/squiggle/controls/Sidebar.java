/*
 * Copyright 2017 Teamninjaneer.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.squiggle.controls;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener.Change;
import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;

/**
 *
 * @author chris.heidt
 */
public class Sidebar extends Region {

    private final BorderPane layout;
    private final Pane handlebar;
    private final ObjectProperty<HandlebarPosition> handlebarPositionProperty = new SimpleObjectProperty<>(null);
    private final ObservableList<SidebarItem> sidebarItems = FXCollections.<SidebarItem>observableArrayList();
    public final static String DEFAULT_HANDLE_TEXT = "* * *";

    /**
     * ENUM for handlebar positioning
     */
    public enum HandlebarPosition {
        BOTTOM,
        LEFT,
        RIGHT,
        TOP
    }

    /**
     *
     */
    public Sidebar() {
        super();

        layout = new BorderPane();
        super.getChildren().add(layout);

        // Setup handlebar
        handlebar = new HBox();
        handlebarPositionProperty.addListener((obs, ov, nv) -> repositionHandlebar(nv));
        handlebarPositionProperty.set(HandlebarPosition.RIGHT); // Trigger listener

        // Setup handles
        ToggleGroup toggleGroup = new ToggleGroup();
        toggleGroup.selectedToggleProperty().addListener((obs, ov, nv) -> {
            if (nv == null) {
                layout.setCenter(null);
            } else {
                layout.setCenter((Node) nv.getUserData());
            }
        });

        sidebarItems.addListener((Change<? extends SidebarItem> c) -> {
            while (c.next()) {
                c.getRemoved().stream().map(item -> {
                    item.setToggleGroup(null);
                    return item;
                }).forEach(item -> handlebar.getChildren().remove(item));

                c.getAddedSubList().stream().map(item -> {
                    item.setToggleGroup(toggleGroup);
                    return item;
                }).forEach(item -> handlebar.getChildren().add(item));
            }
        });
    }

    /**
     * Constructor with arguments for Sidebar class
     *
     * @param items
     */
    public Sidebar(SidebarItem... items) {
        this();
        sidebarItems.setAll(items);
    }

    /**
     * Method to expand the sidebar
     *
     * @param name
     */
    public void expand(String name) {
        sidebarItems.stream()
                .filter(item -> item.getText().equals(name))
                .findAny()
                .ifPresent(item -> item.setSelected(true));
    }

    /**
     * Method to collapse the sidebar
     */
    public void collapse() {
        sidebarItems.stream().forEach(item -> item.setSelected(false));
    }

    /**
     * Method to retrieve the handlebar position property of the sidebar
     *
     * @return
     */
    public ObjectProperty<HandlebarPosition> handlebarPositionProperty() {
        return handlebarPositionProperty;
    }

    /**
     * Method to retrieve the handlebar position of the sidebar
     *
     * @return
     */
    public HandlebarPosition getHandlebarPosition() {
        return handlebarPositionProperty.get();
    }

    /**
     * Method to set the handlebar position of the sidebar
     *
     * @param pos
     */
    public void setHandlebarPosition(HandlebarPosition pos) {
        handlebarPositionProperty.set(pos);
    }

    /**
     * Method to retrieve the sidebar contents
     *
     * @return
     */
    public ObservableList<SidebarItem> getItems() {
        return sidebarItems;
    }

    private void repositionHandlebar(HandlebarPosition pos) {
        layout.setLeft(null);
        layout.setTop(null);
        layout.setRight(null);
        layout.setBottom(null);

        // Hide the handle
        if (pos == null) {
            return;
        }

        // Reposition
        switch (pos) {
            case LEFT:
                handlebar.setRotate(-90);
                layout.setLeft(new Group(handlebar));
                break;
            case RIGHT:
                handlebar.setRotate(90);
                layout.setRight(new Group(handlebar));
                break;
            case TOP:
                handlebar.setRotate(0);
                layout.setTop(new Group(handlebar));
                break;
            case BOTTOM:
                handlebar.setRotate(0);
                layout.setBottom(new Group(handlebar));
                break;
            default:
                break;
        }
    }
}
