/*
 * Copyright 2017 Teamninjaneer.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.squiggle.controls;

import com.squiggle.LazyPropertyWrapper;
import com.squiggle.Named;
import com.squiggle.Squiggle;
import com.squiggle.SquiggleCoordinate;
import com.squiggle.axes.XAxis;
import com.squiggle.axes.YAxis;
import com.squiggle.render.Coordinate;
import com.squiggle.render.Pickable;
import com.squiggle.render.RenderingContext;
import static com.squiggle.render.RenderingContext.snap;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javafx.animation.AnimationTimer;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.geometry.Insets;
import javafx.scene.Cursor;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Chris Heidt <chris.heidt@vencore.com>
 */
public class SquigglePlot extends Region implements Named {

    private static final Logger LOGGER = LoggerFactory.getLogger(SquigglePlot.class);

    //animation constants
    private static final int FRAMES_PER_SECOND = 30;
    private static final double NANOS_PER_FRAME = 1e9 / FRAMES_PER_SECOND;
    private static final double ZOOM_FACTOR = 0.05;

    //draw constants
    private static final Font DEFAULT_FONT = Font.getDefault();
    private static final Font BOLD_FONT = Font.font(null, FontWeight.BOLD, -1);
    private static final double CROSSHAIR_WIDTH = 1;
    private static final Color CROSSHAIR_COLOR = Color.GOLD;
    private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#.###");

    private final List<XAxis> xAxes = new ArrayList<>();
    private final List<YAxis> yAxes = new ArrayList<>();
    private final List<Squiggle> squiggles = new ArrayList<>();
    private final BooleanProperty controlsEnabledProperty = new SimpleBooleanProperty(false);
    private final LazyPropertyWrapper<String> nameLazyProperty;
    private final RenderingContext ctx;
    private final AnimationTimer animationTimer;
    private final EventHandler<? super MouseEvent> mouseClickedEventHandler;
    private final EventHandler<? super MouseEvent> mouseMovedEventHandler;
    private final EventHandler<? super MouseEvent> mousePressedEventHandler;
    private final EventHandler<? super MouseEvent> mouseDraggedEventHandler;
    private final EventHandler<? super MouseEvent> mouseReleasedEventHandler;
    private final EventHandler<? super ScrollEvent> scrollEventHandler;
    private Coordinate screenCursorPosition = null;
    private boolean dirty = true;
    private boolean enableAxis = true;
    private boolean enableCrosshairs = true;

    public SquigglePlot() {
        this(null);
    }

    public SquigglePlot(String name) {
        super();
        this.getStyleClass().add("squiggle-plot");
        this.setCursor(Cursor.NONE);

        nameLazyProperty = new LazyPropertyWrapper<>(SimpleStringProperty.class, name);

        // setup canvases
        Canvas mainCanvas = new Canvas();
        Canvas pickingCanvas = new Canvas();

        //bind sizes
        mainCanvas.widthProperty().bind(widthProperty());
        mainCanvas.heightProperty().bind(heightProperty());
        pickingCanvas.widthProperty().bind(widthProperty());
        pickingCanvas.heightProperty().bind(heightProperty());

        super.getChildren().addAll(mainCanvas);

        //redraw on size changes
        mainCanvas.widthProperty().addListener(obs -> redraw());//schedule redraw
        mainCanvas.heightProperty().addListener(obs -> redraw());//schedule redraw

        //setup rendering context
        ctx = new RenderingContext(mainCanvas.getGraphicsContext2D());
        ctx.setInsets(new Insets(0, 0, 20.5, 20.5));

        //Clicked handler
        mouseClickedEventHandler = mouseClickedEvent -> {
            //prevent click on drag
            if (!mouseClickedEvent.isStillSincePress()) {
                return;
            }

            Coordinate pickCoordinate = new Coordinate(mouseClickedEvent.getX(), mouseClickedEvent.getY());

            //copy ctx but use picking gc
            RenderingContext pickCtx = new RenderingContext(pickingCanvas.getGraphicsContext2D(), ctx);
            pickCtx.setPickCoordinate(pickCoordinate);
            render(pickCtx);//render to pick canvas

            Pickable pickable = pickCtx.resolvePick();
            if (pickable != null) {
                pickable.pick(pickCoordinate);
                redraw();//schedule redraw
            }
        };

        //Moved handler
        mouseMovedEventHandler = mouseMovedEvent -> {
            screenCursorPosition = new Coordinate(mouseMovedEvent.getX(), mouseMovedEvent.getY());
            redraw();//schedule redraw
        };

        //Press handler
        mousePressedEventHandler = mousePressedEvent -> {
            this.setCursor(Cursor.MOVE);
            screenCursorPosition = new Coordinate(mousePressedEvent.getX(), mousePressedEvent.getY());
        };

        //Drag handler
        mouseDraggedEventHandler = mouseDraggedEvent -> {
            double deltaX = mouseDraggedEvent.getX() - screenCursorPosition.getX();
            double deltaY = screenCursorPosition.getY() - mouseDraggedEvent.getY();

            ctx.setTranslateX(ctx.getTranslateX() + deltaX);
            ctx.setTranslateY(ctx.getTranslateY() + deltaY);

            screenCursorPosition = new Coordinate(mouseDraggedEvent.getX(), mouseDraggedEvent.getY());

            redraw();//schedule redraw
        };

        //Released handler
        mouseReleasedEventHandler = mouseReleasedEvent -> {
            this.setCursor(Cursor.NONE);
        };

        //scroll handler
        scrollEventHandler = scrollEvent -> {
            int zoomDirection = (scrollEvent.getDeltaY() > 0) ? 1 : -1;

            if (!scrollEvent.isAltDown()) {//alt prevents x scaling
                double scaleX = ctx.getScaleX() + (zoomDirection * ZOOM_FACTOR);
                ctx.setScaleX(scaleX < ZOOM_FACTOR ? ZOOM_FACTOR : scaleX);
            }
            if (!scrollEvent.isControlDown()) {//ctrl prevents y scaling
                double scaleY = ctx.getScaleY() + (zoomDirection * ZOOM_FACTOR);
                ctx.setScaleY(scaleY < ZOOM_FACTOR ? ZOOM_FACTOR : scaleY);
            }

            redraw();//schedule redraw
        };

        //enable/disable mouse controls
        mainCanvas.addEventHandler(MouseEvent.MOUSE_MOVED, mouseMovedEventHandler);//always on
        controlsEnabledProperty.addListener((obs, ov, enabled) -> {
            if (enabled) {
                mainCanvas.addEventHandler(MouseEvent.MOUSE_CLICKED, mouseClickedEventHandler);
                mainCanvas.addEventHandler(MouseEvent.MOUSE_PRESSED, mousePressedEventHandler);
                mainCanvas.addEventHandler(MouseEvent.MOUSE_DRAGGED, mouseDraggedEventHandler);
                mainCanvas.addEventHandler(MouseEvent.MOUSE_RELEASED, mouseReleasedEventHandler);
                mainCanvas.addEventHandler(ScrollEvent.SCROLL, scrollEventHandler);
            } else {
                mainCanvas.removeEventHandler(MouseEvent.MOUSE_CLICKED, mouseClickedEventHandler);
                mainCanvas.removeEventHandler(MouseEvent.MOUSE_PRESSED, mousePressedEventHandler);
                mainCanvas.removeEventHandler(MouseEvent.MOUSE_DRAGGED, mouseDraggedEventHandler);
                mainCanvas.removeEventHandler(MouseEvent.MOUSE_RELEASED, mouseReleasedEventHandler);
                mainCanvas.removeEventHandler(ScrollEvent.SCROLL, scrollEventHandler);
            }
        });

        //setup animation looop
        animationTimer = new AnimationTimer() {

            long previousNanoTime = 0;

            @Override
            public void handle(long currentNanoTime) {
                //enforce FPS
                if (currentNanoTime - previousNanoTime < NANOS_PER_FRAME) {
                    return;
                }
                previousNanoTime = currentNanoTime;

                if (dirty) {
                    render(ctx);
                    dirty = false;
                }
            }
        };

        //let er rip!
        animationTimer.start();
    }

    public void redraw() {
        dirty = true;
    }

    public void forceRedraw() {
        render(ctx);//force an out of animation loop rendering
    }

    public void destroy() {
        animationTimer.stop();
    }

    public List<XAxis> getXAxesReadOnly() {
        return Collections.unmodifiableList(new ArrayList(xAxes));
    }

    public void addXAxis(XAxis xAxis) {
        xAxes.add(xAxis);
        redraw();
    }

    public void removeXAxis(XAxis xAxis) {
        xAxes.remove(xAxis);
        redraw();
    }

    public List<YAxis> getYAxesReadOnly() {
        return Collections.unmodifiableList(new ArrayList(yAxes));
    }

    public void addYAxis(YAxis yAxis) {
        yAxes.add(yAxis);
        redraw();
    }

    public void removeYAxis(YAxis xAxis) {
        yAxes.remove(xAxis);
        redraw();
    }

    public List<Squiggle> getSquigglesReadOnly() {
        return Collections.unmodifiableList(new ArrayList(squiggles));
    }

    public void addSquiggle(Squiggle squiggle) {
        squiggles.add(squiggle);
        redraw();
    }

    public void removeSquiggle(Squiggle squiggle) {
        squiggles.remove(squiggle);
        redraw();
    }

    public Property<Boolean> controlsEnabledProperty() {
        return controlsEnabledProperty;
    }

    public boolean isControlsEnabled() {
        return controlsEnabledProperty.getValue();
    }

    public void setControlsEnabled(boolean enabled) {
        controlsEnabledProperty.setValue(enabled);
    }

    public boolean isAxisEnabled() {
        return enableAxis;
    }

    public void setAxisEnabled(boolean enabled) {
        this.enableAxis = enabled;
        redraw();
    }

    public boolean isCrosshairsEnabled() {
        return enableCrosshairs;
    }

    public void setCrosshairsEnabled(boolean enabled) {
        this.enableCrosshairs = enabled;
        redraw();
    }

    @Override
    public Property<String> nameProperty() {
        return nameLazyProperty.getProperty();
    }

    @Override
    public String getName() {
        return nameLazyProperty.getValue();
    }

    @Override
    public void setName(String name) {
        nameLazyProperty.setValue(name);
    }

    private void render(RenderingContext ctx) {
        //reset the canvases
        ctx.clear();

        //draw each squiggle
        int i;
        for (i = 0; i < squiggles.size(); i++) {
            squiggles.get(i).render(ctx);
        }

        //Axes
        if (enableAxis) {
            for (i = 0; i < xAxes.size(); i++) {
                xAxes.get(i).render(ctx);
            }
            for (i = 0; i < yAxes.size(); i++) {
                yAxes.get(i).render(ctx);
            }
        }

        //Crosshairs
        if (enableCrosshairs && !ctx.isPicking()) {
            renderCrosshairs(ctx);
        }
    }

    private void renderCrosshairs(RenderingContext ctx) {
        Coordinate screenOrigin = ctx.getScreenOrigin();
        Coordinate plotOrigin = ctx.getPlotOrigin();
        Coordinate cartOrigin = ctx.getCartOrigin();

        GraphicsContext gc = ctx.getGraphicsContext();
        gc.save();

        //origin
        gc.setLineWidth(1);
        gc.setStroke(Color.WHITE);
        gc.beginPath();
        //prevent horizontal crosssection from displaying over x axis
        if (cartOrigin.getY() < ctx.getPlotHeight()) {
            gc.moveTo(snap(plotOrigin.getX()), snap(cartOrigin.getY()));
            gc.lineTo(snap(plotOrigin.getX() + ctx.getPlotWidth()), snap(cartOrigin.getY()));
        }
        //prevent vertical crosssection from displaying over y axis
        if (cartOrigin.getX() > plotOrigin.getX()) {
            gc.moveTo(snap(cartOrigin.getX()), snap(plotOrigin.getY()));
            gc.lineTo(snap(cartOrigin.getX()), snap(plotOrigin.getY() + ctx.getPlotHeight()));
        }
        gc.stroke();

        //cursor position
        if (screenCursorPosition != null) {
            //cursor position crosshair
            gc.setLineWidth(CROSSHAIR_WIDTH);
            gc.setStroke(CROSSHAIR_COLOR);
            gc.beginPath();

            //prevent horizontal crosssection from displaying over x axis
            if (screenCursorPosition.getY() < plotOrigin.getY() + ctx.getPlotHeight()) {
                gc.moveTo(snap(plotOrigin.getX()), snap(screenCursorPosition.getY()));
                gc.lineTo(snap(plotOrigin.getX() + ctx.getPlotWidth()), snap(screenCursorPosition.getY()));
            }

            //prevent vertical crosssection from displaying over y axis
            if (screenCursorPosition.getX() > plotOrigin.getX()) {
                gc.moveTo(snap(screenCursorPosition.getX()), snap(plotOrigin.getY()));
                gc.lineTo(snap(screenCursorPosition.getX()), snap(plotOrigin.getY() + ctx.getPlotHeight()));
            }
            gc.stroke();

            //cartesian cursor position
            Coordinate cartCursorPosition = ctx.screenToCart(screenCursorPosition.getX(), screenCursorPosition.getY());

            //X pos text
            String xPosText = DECIMAL_FORMAT.format(cartCursorPosition.getX());
            Bounds xPosTextBounds = new Text(xPosText).getLayoutBounds();

            //X pos text background fill
            gc.setFill(Color.BLACK);
            gc.fillRect(screenCursorPosition.getX() - xPosTextBounds.getWidth() / 2, ctx.getScreenHeight() - BOLD_FONT.getSize(), xPosTextBounds.getWidth(), BOLD_FONT.getSize());

            //X pos text fill
            gc.setFont(BOLD_FONT);
            gc.setFill(CROSSHAIR_COLOR);
            gc.setTextAlign(TextAlignment.CENTER);
            gc.fillText(xPosText, screenCursorPosition.getX(), ctx.getScreenHeight());

            //Y pos text
            String yPosText = DECIMAL_FORMAT.format(cartCursorPosition.getY());
            Bounds yPosTextBounds = new Text(yPosText).getLayoutBounds();

            //rotate
            gc.save();
            gc.translate(screenOrigin.getX(), screenCursorPosition.getY());
            gc.rotate(90);

            //Y pos text background fill
            gc.setFill(Color.BLACK);
            gc.fillRect(0 - yPosTextBounds.getWidth() / 2, 0 - BOLD_FONT.getSize(), yPosTextBounds.getWidth(), yPosTextBounds.getHeight());

            //Y pos text fill
            gc.setFill(CROSSHAIR_COLOR);
            gc.fillText(DECIMAL_FORMAT.format(cartCursorPosition.getY()), 0, 0);//0,0 is origin of rotation

            //un-rotate
            gc.restore();
        }

        gc.restore();
    }

    public double getMaxX() {
        return squiggles.stream()
                .map(Squiggle::getCoordinatesReadOnly).flatMap(coords -> coords.stream())
                .mapToDouble(SquiggleCoordinate::getX)
                .max().getAsDouble();
    }

    public double getMaxY() {
        return squiggles.stream()
                .map(Squiggle::getCoordinatesReadOnly).flatMap(coords -> coords.stream())
                .mapToDouble(SquiggleCoordinate::getY)
                .max().getAsDouble();
    }
}
