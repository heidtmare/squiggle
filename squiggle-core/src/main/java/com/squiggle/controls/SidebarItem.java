/*
 * Copyright 2017 Teamninjaneer.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.squiggle.controls;

import javafx.scene.Node;
import javafx.scene.control.ToggleButton;

/**
 *
 * @author Chris Heidt <chris.heidt@vencore.com>
 */
public class SidebarItem extends ToggleButton {

    public SidebarItem() {
        super();
    }

    public SidebarItem(String text) {
        super(text);
    }

    /**
     * Constructor with arguments for SidebarItem class
     *
     * @param text
     * @param graphic
     */
    public SidebarItem(String text, Node graphic) {
        super(text, graphic);
    }

    /**
     * Constructor with arguments for SidebarItem class
     *
     * @param text
     * @param graphic
     * @param content
     */
    public SidebarItem(String text, Node graphic, Node content) {
        super(text, graphic);
        setUserData(content);
    }

    /**
     * Method to retrieve the node of content from the sidebar item
     *
     * @return
     */
    public Node getContent() {
        return (Node) getUserData();
    }
}
