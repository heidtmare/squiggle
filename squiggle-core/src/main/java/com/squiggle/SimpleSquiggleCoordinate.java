/*
 * Copyright 2017 Teamninjaneer.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.squiggle;

import com.squiggle.render.RenderableBase;
import com.squiggle.render.Coordinate;
import com.squiggle.render.RenderingAttributes;
import com.squiggle.render.RenderingContext;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 *
 * @author Chris Heidt <chris.heidt@vencore.com>
 */
public class SimpleSquiggleCoordinate extends RenderableBase implements SquiggleCoordinate {

    static {
        defaultRenderingAttributes = new RenderingAttributes.Builder().withFillPaint(Color.CYAN).build();
    }

    private final double x;
    private final double y;
    private final double z;
    private final double m;

    public SimpleSquiggleCoordinate(double x, double y) {
        this.x = x;
        this.y = y;
        this.z = Double.NaN;
        this.m = Double.NaN;
    }

    public SimpleSquiggleCoordinate(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.m = Double.NaN;
    }

    public SimpleSquiggleCoordinate(double x, double y, double z, double m) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.m = m;
    }

    @Override
    public double getX() {
        return x;
    }

    @Override
    public double getY() {
        return y;
    }

    @Override
    public double getZ() {
        return z;
    }

    @Override
    public double getM() {
        return m;
    }

    @Override
    public void render(RenderingContext ctx) {
        GraphicsContext gc = ctx.getGraphicsContext();
        gc.save();

        //apply rendering attributes
        if (renderingAttributes != null) {
            renderingAttributes.apply(gc);
        } else {
            defaultRenderingAttributes.apply(gc);
        }

        //apply pick color
        if (ctx.isPicking()) {
            Color pickColor = ctx.registerPickable(this);
            gc.setStroke(pickColor);
            gc.setFill(pickColor);
        }

        Coordinate screen = ctx.cartToScreen(this.getX(), this.getY());
        gc.moveTo(screen.getX(), screen.getY());
        gc.fillOval(screen.getX() - 2.5, screen.getY() - 2.5, 5, 5);

        gc.restore();
    }

    @Override
    public void pick(Coordinate pickCoordinate) {
        System.out.println("A SquiggleCoordinate was picked!");
    }
    
    
}
