/*
 * Copyright 2017 Teamninjaneer.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.squiggle.application;

import com.squiggle.SimpleSquiggle;
import com.squiggle.SimpleSquiggleCoordinate;
import com.squiggle.SquiggleCoordinate;
import com.squiggle.axes.LinearXAxis;
import com.squiggle.axes.LinearYAxis;
import com.squiggle.controls.Sidebar;
import com.squiggle.controls.SidebarItem;
import com.squiggle.controls.SquigglePlot;
import com.squiggle.render.RenderingAttributes;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.MenuBar;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;

/**
 *
 * @author Chris Heidt <chris.heidt@vencore.com>
 */
public class SquiggleApplicationView extends StackPane {

    private static final int SQUIGGLE_COORDINATE_COUNT = 1000;
    private static final int SPACER = 10;
    private final Random random = new Random();

    @FXML
    private MenuBar menu;

    @FXML
    private ToolBar toolbar;

    @FXML
    private Sidebar sidebar;

    @FXML
    private SquigglePlot plot;

    @FXML
    private Button addSquiggleBtn;

    public SquiggleApplicationView() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("SquiggleApplicationView.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    public void initialize() {
        Pane pane = new Pane();
        pane.setPrefWidth(300);
        sidebar.setHandlebarPosition(Sidebar.HandlebarPosition.LEFT);
        sidebar.getItems().add(new SidebarItem("Squiggles", null, pane));

        plot.setControlsEnabled(true);
        plot.setAxisEnabled(true);
        plot.setCrosshairsEnabled(true);
        plot.addXAxis(new LinearXAxis("My X Axis"));
        plot.addYAxis(new LinearYAxis("My Y Axis"));

        addSquiggleBtn.setOnAction((e) -> {
            int count = plot.getSquigglesReadOnly().size();
            List<SquiggleCoordinate> coordinates = new ArrayList<>();
            for (int j = 0; j < SQUIGGLE_COORDINATE_COUNT; j++) {
                double x = j * SPACER;
                double y = (count * SPACER) + (Math.sin(x) * SPACER);
                coordinates.add(new SimpleSquiggleCoordinate(x, y));
            }

            Color randColor = randColor(random);
            SimpleSquiggle squiggle = new SimpleSquiggle("Squiggle " + count, coordinates);
            squiggle.setRenderingAttributes(new RenderingAttributes.Builder()
                    .withStrokePaint(randColor)
                    .withFillPaint(randColor)
                    .withLineWidth(2)
                    .build());
            plot.addSquiggle(squiggle);
        });
    }

    private Color randColor(Random random) {
        return new Color(random.nextDouble(), random.nextDouble(), random.nextDouble(), 1);
    }
}
