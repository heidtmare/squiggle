/*
 * Copyright 2017 Teamninjaneer.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.squiggle.application;

import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Example of using the waveform plot to put a waveform into a scene
 *
 * @author Chris Heidt <chris.heidt@vencore.com>
 */
public class SquiggleApplication extends Application {

    private static final String THEME = "squiggle-dark-theme.css";

    @Override
    public void start(Stage stage) throws Exception {

        //build out the main view
        SquiggleApplicationView view = new SquiggleApplicationView();
        view.setPrefSize(1152, 648);

        Scene scene = new Scene(view);
        scene.getStylesheets().add(getClass().getResource(THEME).toExternalForm());

        //standard stage stuff
        stage.setTitle("Squiggle Application");
        stage.setScene(scene);
        stage.show();
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application. main() serves only as
     * fallback in case the application can not be launched through deployment artifacts, e.g., in
     * IDEs with limited FX support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
